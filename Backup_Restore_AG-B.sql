--:setvar DBID 5ubbc4sfvfb
--:setvar APPNAME timecard
--:setvar SIDT 0x00000000000000000000000000000001
--:setvar SIDP 0x00000000000000000000000000000002

:Connect DAN1W05
if exists(select * from master.[sys].[availability_groups] where name = '$(APPNAME)-ag')
BEGIN
	DROP AVAILABILITY GROUP [$(APPNAME)-ag];
END;
GO
if exists(select * from master.sys.syslogins where name='tenant-$(DBID)')
begin
   DROP LOGIN [tenant-$(DBID)]
end
GO
CREATE LOGIN [tenant-$(DBID)] WITH PASSWORD=N'$(PASSWORDT)', CHECK_EXPIRATION=ON, CHECK_POLICY=ON, DEFAULT_DATABASE=[master],SID = $(SIDT)
GO
if exists(select * from master.sys.syslogins where name='provider-$(DBID)')
BEGIN
	DROP LOGIN [provider-$(DBID)]
END
GO
CREATE LOGIN [provider-$(DBID)] WITH PASSWORD=N'$(PASSWORDP)', CHECK_EXPIRATION=ON, CHECK_POLICY=ON, DEFAULT_DATABASE=[master],SID = $(SIDP)
GO

EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'$(APPNAME)__$(DBID)'
GO
USE [master]
GO
if exists (select * from master.sys.databases where name='$(APPNAME)__$(DBID)' and state_desc='ONLINE')
BEGIN
	ALTER DATABASE [$(APPNAME)__$(DBID)] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
END
GO
if exists (select * from master.sys.databases where name='$(APPNAME)__$(DBID)')
BEGIN
	DROP DATABASE [$(APPNAME)__$(DBID)]
END
GO

USE [master]
RESTORE DATABASE [$(APPNAME)__$(DBID)] FROM  DISK = N'\\DAN1W01\Partitions\DAN1-FileShare\$(APPNAME)__$(DBID)_backup' WITH  FILE = 1, REPLACE,  NOUNLOAD,  STATS = 5
GO


--- YOU MUST EXECUTE THE FOLLOWING SCRIPT IN SQLCMD MODE.
:Connect DAN1W05

IF (SELECT state FROM sys.endpoints WHERE name = N'Hadr_endpoint') <> 0
BEGIN
	ALTER ENDPOINT [Hadr_endpoint] STATE = STARTED
END


GO

use [master]

GO

GRANT CONNECT ON ENDPOINT::[Hadr_endpoint] TO [cloud\MSSQLService]

GO

:Connect DAN1W05

IF EXISTS(SELECT * FROM sys.server_event_sessions WHERE name='AlwaysOn_health')
BEGIN
  ALTER EVENT SESSION [AlwaysOn_health] ON SERVER WITH (STARTUP_STATE=ON);
END
IF NOT EXISTS(SELECT * FROM sys.dm_xe_sessions WHERE name='AlwaysOn_health')
BEGIN
  ALTER EVENT SESSION [AlwaysOn_health] ON SERVER STATE=START;
END

GO

:Connect DAN1W06

IF (SELECT state FROM sys.endpoints WHERE name = N'Hadr_endpoint') <> 0
BEGIN
	ALTER ENDPOINT [Hadr_endpoint] STATE = STARTED
END


GO

use [master]

GO

GRANT CONNECT ON ENDPOINT::[Hadr_endpoint] TO [cloud\MSSQLService]

GO

:Connect DAN1W06

IF EXISTS(SELECT * FROM sys.server_event_sessions WHERE name='AlwaysOn_health')
BEGIN
  ALTER EVENT SESSION [AlwaysOn_health] ON SERVER WITH (STARTUP_STATE=ON);
END
IF NOT EXISTS(SELECT * FROM sys.dm_xe_sessions WHERE name='AlwaysOn_health')
BEGIN
  ALTER EVENT SESSION [AlwaysOn_health] ON SERVER STATE=START;
END

GO

:Connect DAN1W05

USE [master]

GO

CREATE AVAILABILITY GROUP [$(APPNAME)-ag]
WITH (AUTOMATED_BACKUP_PREFERENCE = SECONDARY,
DB_FAILOVER = OFF,
DTC_SUPPORT = NONE)
FOR DATABASE [$(APPNAME)__$(DBID)]
REPLICA ON N'DAN1W05' WITH (ENDPOINT_URL = N'TCP://DAN1W05.cloud.Apprenda.local:5022', FAILOVER_MODE = AUTOMATIC, AVAILABILITY_MODE = SYNCHRONOUS_COMMIT, BACKUP_PRIORITY = 50, SECONDARY_ROLE(ALLOW_CONNECTIONS = NO)),
	N'DAN1W06' WITH (ENDPOINT_URL = N'TCP://DAN1W06.cloud.Apprenda.local:5022', FAILOVER_MODE = AUTOMATIC, AVAILABILITY_MODE = SYNCHRONOUS_COMMIT, BACKUP_PRIORITY = 50, SECONDARY_ROLE(ALLOW_CONNECTIONS = NO));

GO

:Connect DAN1W05

USE [master]

GO

ALTER AVAILABILITY GROUP [$(APPNAME)-ag]
ADD LISTENER N'DAN1_L' (
WITH IP
((N'$(IPLISTENER)', N'255.255.192.0')
)
, PORT=$(PORTLISTENER));

GO

:Connect DAN1W06

ALTER AVAILABILITY GROUP [$(APPNAME)-ag] JOIN;

GO

:Connect DAN1W05

BACKUP DATABASE [$(APPNAME)__$(DBID)] TO  DISK = N'\\DAN1W01\Partitions\$(APPNAME)__$(DBID).bak' WITH  COPY_ONLY, FORMAT, INIT, SKIP, REWIND, NOUNLOAD, COMPRESSION,  STATS = 5

GO

:Connect DAN1W06
if exists(select * from master.sys.syslogins where name='tenant-$(DBID)')
begin
   DROP LOGIN [tenant-$(DBID)]
end
GO
CREATE LOGIN [tenant-$(DBID)] WITH PASSWORD=N'$(PASSWORDT)', CHECK_EXPIRATION=ON, CHECK_POLICY=ON, DEFAULT_DATABASE=[master],SID = $(SIDT)
GO
if exists(select * from master.sys.syslogins where name='provider-$(DBID)')
BEGIN
	DROP LOGIN [provider-$(DBID)]
END
GO
CREATE LOGIN [provider-$(DBID)] WITH PASSWORD=N'$(PASSWORDP)', CHECK_EXPIRATION=ON, CHECK_POLICY=ON, DEFAULT_DATABASE=[master],SID = $(SIDP)
GO

EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'$(APPNAME)__$(DBID)'
GO
USE [master]
GO
if exists (select * from master.sys.databases where name='$(APPNAME)__$(DBID)' and state_desc='ONLINE')
BEGIN
	ALTER DATABASE [$(APPNAME)__$(DBID)] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
END
GO
if exists (select * from master.sys.databases where name='$(APPNAME)__$(DBID)')
BEGIN
	DROP DATABASE [$(APPNAME)__$(DBID)]
END
GO

RESTORE DATABASE [$(APPNAME)__$(DBID)] FROM  DISK = N'\\DAN1W01\Partitions\$(APPNAME)__$(DBID).bak' WITH  NORECOVERY,  NOUNLOAD,  STATS = 5
GO

/*:Connect DAN1W05
BACKUP LOG [$(APPNAME)__$(DBID)] TO  DISK = N'\\DAN1W01\Partitions\$(APPNAME)__$(DBID)_20180118204016.trn' WITH NOFORMAT, NOINIT, NOSKIP, REWIND, NOUNLOAD, COMPRESSION,  STATS = 5
GO
:Connect DAN1W06
RESTORE LOG [$(APPNAME)__$(DBID)] FROM  DISK = N'\\DAN1W01\Partitions\$(APPNAME)__$(DBID)_20180118204016.trn' WITH  NORECOVERY,  NOUNLOAD,  STATS = 5
GO*/


:Connect DAN1W06

-- Wait for the replica to start communicating
begin try
declare @conn bit
declare @count int
declare @replica_id uniqueidentifier 
declare @group_id uniqueidentifier
set @conn = 0
set @count = 30 -- wait for 5 minutes 

if (serverproperty('IsHadrEnabled') = 1)
	and (isnull((select member_state from master.sys.dm_hadr_cluster_members where upper(member_name COLLATE Latin1_General_CI_AS) = upper(cast(serverproperty('ComputerNamePhysicalNetBIOS') as nvarchar(256)) COLLATE Latin1_General_CI_AS)), 0) <> 0)
	and (isnull((select state from master.sys.database_mirroring_endpoints), 1) = 0)
begin
    select @group_id = ags.group_id from master.sys.availability_groups as ags where name = N'$(APPNAME)-ag'
	select @replica_id = replicas.replica_id from master.sys.availability_replicas as replicas where upper(replicas.replica_server_name COLLATE Latin1_General_CI_AS) = upper(@@SERVERNAME COLLATE Latin1_General_CI_AS) and group_id = @group_id
	while @conn <> 1 and @count > 0
	begin
		set @conn = isnull((select connected_state from master.sys.dm_hadr_availability_replica_states as states where states.replica_id = @replica_id), 1)
		if @conn = 1
		begin
			-- exit loop when the replica is connected, or if the query cannot find the replica status
			break
		end
		waitfor delay '00:00:10'
		set @count = @count - 1
	end
end
end try
begin catch
	-- If the wait loop fails, do not stop execution of the alter database statement
end catch
ALTER DATABASE [$(APPNAME)__$(DBID)] SET HADR AVAILABILITY GROUP = [$(APPNAME)-ag];

GO




