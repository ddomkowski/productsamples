param
(
    [string] $appname = 'timecard',
    [string] $dbid = '5ubbc4sfvfb',
    [string] $ClusterNetworkName = "Cluster Network 1",
    [int]    $ProbePort = 41234,
    [int]    $ListenerPort = 1536,
    [string] $ListenerIp = "10.20.65.52",
    [string] $providerPassword = $null,
    [string] $tenantPassword = $null
)

function Get-DecryptedString
{
    [CmdletBinding()]
    param
    (
        [SecureString] $secureString
    )

    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureString)
    return [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
}

if ([string]::IsNullOrWhiteSpace($tenantPassword))
{
    $tenantAccount = Get-Credential "tenant-$dbid"
    $tenantPassword = Get-DecryptedString -secureString $tenantAccount.Password
}

if ([string]::IsNullOrWhiteSpace($providerPassword))
{
    $providerAccount = Get-Credential "provider-$dbid"
    $providerPassword = Get-DecryptedString -secureString $providerAccount.Password
}

sqlcmd -i '.\Backup_Restore_AG-A.sql' "-vDBID=$dbid" "-vAPPNAME=$appname" "-vPASSWORDP=$providerPassword" "-vPASSWORDT=$tenantPassword"

$conn = New-Object System.Data.SqlClient.SqlConnection
$conn.ConnectionString =  "Data Source=DAN1W04;Initial Catalog=master;Integrated Security=true"
$conn.Open()
$cmd = $conn.CreateCommand()
$cmd.CommandText = "select master.dbo.fn_varbintohexstr(sid) from master.sys.syslogins where name='provider-$dbid'"
$sidp = $cmd.ExecuteScalar()
$cmd.CommandText = "select master.dbo.fn_varbintohexstr(sid) from master.sys.syslogins where name='tenant-$dbid'"
$sidt = $cmd.ExecuteScalar()
$conn.Close()
$conn = $null

sqlcmd -i '.\Backup_Restore_AG-B.sql' "-vDBID=$dbid" "-vAPPNAME=$appname" "-vSIDT=$sidt" "-vPASSWORDT=$tenantPassword" "-vSIDP=$sidp" "-vPASSWORDP=$providerPassword" "-vIPLISTENER=$ListenerIp" "-vPORTLISTENER=$ListenerPort"

$IPResourceName = "$appname-ag_$ListenerIp" 

Import-Module FailoverClusters

Get-ClusterResource $IPResourceName | Set-ClusterParameter -Multiple @{"Address"="$ListenerIp";"ProbePort"=$ProbePort;"SubnetMask"="255.255.255.255";"Network"="$ClusterNetworkName";"EnableDhcp"=0}
Get-ClusterResource $IPResourceName | Stop-ClusterResource
Get-ClusterResource "$appname-ag" | Start-ClusterResource