--:setvar DBID 5ubbc4sfvfb
--:setvar APPNAME timecard

:Connect DAN1W04
if exists(select * from master.sys.syslogins where name='tenant-$(DBID)')
begin
   DROP LOGIN [tenant-$(DBID)]
end
GO
CREATE LOGIN [tenant-$(DBID)] WITH PASSWORD=N'$(PASSWORDT)', CHECK_EXPIRATION=ON, CHECK_POLICY=ON, DEFAULT_DATABASE=[master]
GO
USE [$(APPNAME)__$(DBID)]
GO
if exists(select * from sys.sysusers where name='tenant-$(DBID)')
BEGIN
	DROP USER [tenant-$(DBID)]
END
GO
CREATE USER [tenant-$(DBID)] FOR LOGIN [tenant-$(DBID)]
GO
ALTER ROLE [TenantRole] ADD MEMBER [tenant-$(DBID)]

GO
if exists(select * from master.sys.syslogins where name='provider-$(DBID)')
begin
   DROP LOGIN [provider-$(DBID)]
end
GO
CREATE LOGIN [provider-$(DBID)] WITH PASSWORD=N'$(PASSWORDP)', CHECK_EXPIRATION=ON, CHECK_POLICY=ON, DEFAULT_DATABASE=[master]
																																		  
GO
USE [$(APPNAME)__$(DBID)]
GO
if exists(select * from sys.sysusers where name='provider-$(DBID)')
BEGIN
	DROP USER [provider-$(DBID)]
END
GO
CREATE USER [provider-$(DBID)] FOR LOGIN [provider-$(DBID)]
GO
ALTER ROLE [db_owner] ADD MEMBER [provider-$(DBID)]
GO

BACKUP DATABASE [$(APPNAME)__$(DBID)] TO  DISK = N'\\DAN1W01\Partitions\DAN1-FileShare\$(APPNAME)__$(DBID)_backup' WITH NOFORMAT, INIT,  NAME = N'$(APPNAME)__$(DBID)-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO
